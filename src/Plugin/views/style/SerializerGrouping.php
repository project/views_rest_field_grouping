<?php

namespace Drupal\views_rest_field_grouping\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rest\Plugin\views\style\Serializer;

/**
 * Defines a custom Views style plugin for REST export with field grouping.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "serializer_grouping",
 *   title = @Translation("Serializer(With Grouping)"),
 *   help = @Translation("Exports grouped results in REST format."),
 *   display_types = {"data"}
 * )
 */
class SerializerGrouping extends Serializer {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['formats'] = ['default' => []];
    $options['group_field'] = ['default' => []];
    $options['group_field_first_letter'] = ['default' => FALSE];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $options = ['' => $this->t('- None -')];
    $field_labels = $this->displayHandler->getFieldLabels(TRUE);
    $options += $field_labels;
    // If there are no fields, we can't group on them.
    if (count($options) > 1) {
      // This is for backward compatibility, when there was just a single
      // select form.
      if (is_string($this->options['grouping'])) {
        $grouping = $this->options['grouping'];
        $this->options['grouping'] = [];
        $this->options['grouping'][0]['field'] = $grouping;
      }
      if (isset($this->options['group_rendered']) && is_string($this->options['group_rendered'])) {
        $this->options['grouping'][0]['rendered'] = $this->options['group_rendered'];
        unset($this->options['group_rendered']);
      }
      $form['group_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Grouping field'),
        '#options' => $options,
        '#default_value' => $this->options['group_field'],
        '#description' => $this->t('You may optionally specify a field by which to group the records. Only level 1 grouping supported'),
      ];
      $form['group_field_first_letter'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Group by first letter'),
        '#default_value' => $grouping['rendered'],
        '#description' => $this->t('If enabled, result will be grouped by the first letter of the selected field value'),
        '#states' => [
          'invisible' => [
            ':input[name="style_options[group_field]"]' => ['value' => ''],
          ],
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $rows = [];
    // If the Data Entity row plugin is used, this will be an array of entities
    // which will pass through Serializer to one of the registered Normalizers,
    // which will transform it to arrays/scalars. If the Data field row plugin
    // is used, $rows will not contain objects and will pass directly to the
    // Encoder.
    foreach ($this->view->result as $row_index => $row) {

      $this->view->row_index = $row_index;

      if (empty($this->options['group_field'])) {
        $rows[] = $this->view->rowPlugin->render($row);
      }
      // If Group by option is enabled.
      else {
        $groupFieldValue = trim($this->getFieldValue($row_index, $this->options['group_field']));
        // If Group by first letter is enabled.
        if (!empty($this->options['group_field_first_letter'])) {
          $firstLetter = substr($groupFieldValue, 0, 1);
          $firstLetter = strtoupper($firstLetter);
          $rows[$firstLetter][] = $this->view->rowPlugin->render($row);
        }
        else {
          $rows[$groupFieldValue][] = $this->view->rowPlugin->render($row);
        }

      }

    }
    // If Group by first letter is enabled,
    // sort the Grouping by its first letter.
    if (!empty($this->options['group_field']) && !empty($this->options['group_field_first_letter'])) {
      ksort($rows);
    }
    unset($this->view->row_index);
    // Get the content type configured in the display or fallback to the
    // default.
    if ((empty($this->view->live_preview))) {
      $content_type = $this->displayHandler->getContentType();
    }
    else {
      $content_type = !empty($this->options['formats']) ? reset($this->options['formats']) : 'json';
    }
    return $this->serializer->serialize($rows, $content_type, ['views_style_plugin' => $this]);

  }

}
