# Views REST Field Grouping

The Views REST Field Grouping module extends the core Views serializer style plugin for REST export. This module provides the ability to group results based on a selected field and also allows grouping by the first letter of the field value. Enhance REST export in Views by enabling customizable grouping of results based on fields.'


## Features

- Group results by a selected field.
- Group results by the first letter of the field value.

## Installation

Install the module using the standard Drupal module installation procedure.


## How to Use

1. Add a new View as you normally would.

2. Add a new display of type 'REST export'.

3. In the 'Format' section, choose 'Serializer(With Grouping)' as the style plugin.

4. Configure the Format settings:
- In the 'Grouping field' option, select a field based on which you want to group the results.
- Check the 'Group by first letter' option if you want to group the results by the first letter of the selected field value.

5. Save the View and test your REST export with the chosen grouping settings.

